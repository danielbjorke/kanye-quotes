import React, { useState } from "react";
import { getQuote } from "../api/quoteService";
import kanyeimg from "../assets/kanye.png";
import { motion } from "framer-motion"



const Quotes = () => {


    const [isLoading, setIsLoading] = useState(false);
    const [quote, setQuote] = useState("");

    const onButtonClicked = async () => {

        setIsLoading(false);
        let quoteresult;

        try {
            quoteresult = await getQuote();
            setQuote(quoteresult.quote);
        } catch (error) {
            console.log(error);
        } finally {
            console.log("Axios request finished")
            setIsLoading(true);
        }
    };

    return (
        <div className="quotes">
            <div className="kanyeinfo">
                <h2>Welcome to Kanye Quotes</h2>
                <h5>Press the button to generate new and insightful quotes from Kanye West.</h5>
                <motion.button
                    whileHover={{ scale: 1.1 }}
                    whileTap={{ scale: 0.9 }}
                    type="button" onClick={onButtonClicked}>THE BUTTON</motion.button>
            </div>

            <div className="kanyequote">
                <motion.img
                    animate={{ rotate: 360 }}
                    transition={{ ease: "linear", duration: 10, repeat: Infinity }}


                    src={kanyeimg} alt="kanyeimage"></motion.img>
                <p>"{quote}"</p>
            </div>
        </div>
    )
}

export default Quotes;