import React from "react";

const Footer = () => {

    return (
        <div className="footer">
            <div>Credit:{ }
                <a href="https://danielbjorke.no/" target="_blank" rel="noopener noreferrer">
                    Daniel Bjørke</a>.
            </div>
            <div>
                To learn more about the API used { }
                <a href="https://kanye.rest/" target="_blank" rel="noopener noreferrer">
                    click here</a>.
            </div>
        </div>
    )
}

export default Footer;