import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.scss';
import Navbar from "./components/navbar";
import Footer from "./components/footer";
import Quotes from "./components/quotes";

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/*" component={Quotes} />
        </Switch>
        <Footer />

      </div>
    </Router>

  );
}

export default App;
