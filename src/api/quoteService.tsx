import environment from '../environment/environment';
import axios from "axios";


export const getQuote = async () => {
    return axios.get(`${environment.apiUrl}`)
        .then(response => response.data);
}
